import React from 'react'

function ListCritereCreationPersonnage() {
    return (
        <div>                
            <label for="species">Espece</label>

            <select name="species" id="species">
            <option value="humain">Humain</option>
            <option value="nain">Nain</option>
            <option value="orc">Orc</option>
            <option value="elfe">Elfe</option>
            </select>
            
            <label for="class">Classe</label>

            <select name="class" id="class">
            <option value="combatant">Combatant</option>
            <option value="archer">Archer</option>
            <option value="mage">Mage</option>
            <option value="voleur">Voleur</option>
            </select>

            <label for="job">Metier</label>

            <select name="job" id="job">
            <option value="bucheron">Bucheron</option>
            <option value="forgeron">Forgeron</option>
            <option value="marchand">Marchand</option>
            <option value="chasseur">Chasseur</option>
            </select>
        </div>
    )
    
  }
  
export default ListCritereCreationPersonnage