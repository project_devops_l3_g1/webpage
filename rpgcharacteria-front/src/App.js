import "./App.css";
import React from "react";
import { useState } from "react";

export default function App(){
  const [species, setSpecies] = useState("");
  const [class_, setClass_] = useState("");
  const [job, setJob] = useState("");
  const [urls, setUrls] = useState("");

  async function onSubmit(event) {
    event.preventDefault();
    try {
      // process.env.API_URL + 
      const response = await fetch("http://localhost:8080/api/generate", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify( {
          species,
          class_,
          job,
        }),
      });
      const data = await response.json();
      if (response.status !== 200) {
        throw (
          data.error || new Error(`Request failed with status ${response.status}`)
        );
      }
  

    } catch (error) {
      // Consider implementing your own error handling logic here
      console.error(error);
      alert(error.message);
    }
    for (let i = 0; i < data.result.length; i++) {
      const element = data.result[i];
      setUrls(urls+element["url"]);
     }
  }
  return (
    <div className="App">
      <form onSubmit={onSubmit}>
      <div>                
            <label for="species">Espece</label>

            <input
            type="text"
            name="species"
            placeholder="Enter a species"
            value={species}
            onChange={(e) => setSpecies(e.target.value)}
          />
            
            <label for="class">Classe</label>

           
            <input
            type="text"
            name="class_"
            placeholder="Enter a class"
            value={class_}
            onChange={(e) => setClass_(e.target.value)}
          />

            <label for="job">Metier</label>

            <input
            type="text"
            name="job"
            placeholder="Enter a job"
            value={job}
            onChange={(e) => setJob(e.target.value)}
          />
        </div>
        <input type="submit" value="Generate" />
      </form>
      <div>{urls}</div>
    </div>
  );
}

